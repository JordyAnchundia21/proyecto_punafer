package jk.tnr.firebaseapp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
public class DatosUsuario {
    private static DatosUsuario DatosUsuario;
    DatosUsuario() {
        FirebaseDatabase database = FirebaseDatabase.getInstance();
        //FirebaseStorage storage = FirebaseStorage.getInstance();
        DatabaseReference referenceUsuarios = database.getReference("Usuarios");
        //      StorageReference referenceFotoDePerfil = storage.getReference("Fotos/FotoPerfil/" + IdUsuario());
    }
    public static DatosUsuario getInstancia() {
        if (DatosUsuario == null) DatosUsuario = new DatosUsuario();
        return DatosUsuario;
    }
    public String IdUsuario() {
        return FirebaseAuth.getInstance().getUid();
    }
    public boolean isUsuarioLogeado() {
        FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        return firebaseUser != null;
    }
    public long fechaDeCreacionLong() {
        return FirebaseAuth.getInstance().getCurrentUser().getMetadata().getCreationTimestamp();
    }
    public long fechaDeUltimaVezQueSeLogeoLong() {
        return FirebaseAuth.getInstance().getCurrentUser().getMetadata().getLastSignInTimestamp();
    }
}
