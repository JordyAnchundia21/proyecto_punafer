package jk.tnr.firebaseapp;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.ocpsoft.prettytime.PrettyTime;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
class AdapterMensajes extends RecyclerView.Adapter<AdapterMensaje> {
    private final List<ModelMensajeRecibir> listMensaje = new ArrayList<>();
    private final Context c;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    public AdapterMensajes(Context c) {
        this.c = c;
    }
    public void addMensaje(ModelMensajeRecibir m) {
        listMensaje.add(m);
        notifyItemInserted(listMensaje.size());
    }
    @Override
    public AdapterMensaje onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (viewType == 1) {
            view = LayoutInflater.from(c).inflate(R.layout.cardview_mensajes_emisor, parent, false);
        } else {
            view = LayoutInflater.from(c).inflate(R.layout.cardview_mensajes_receptor, parent, false);
        }
        return new AdapterMensaje(view);
    }
    @Override
    public void onBindViewHolder(final AdapterMensaje holder, int position) {
        holder.getNombre().setText(listMensaje.get(position).getNombre());
        holder.getMensaje().setText(listMensaje.get(position).getMensaje());
        if (listMensaje.get(position).getType_mensaje().equals("2")) {
            holder.getFotoMensaje().setVisibility(View.VISIBLE);
            holder.getMensaje().setVisibility(View.VISIBLE);
            Glide.with(c).load(listMensaje.get(position).getUrlFoto()).into(holder.getFotoMensaje());
        } else if (listMensaje.get(position).getType_mensaje().equals("1")) {
            holder.getFotoMensaje().setVisibility(View.GONE);
            holder.getMensaje().setVisibility(View.VISIBLE);
        }
        if (listMensaje.get(position).getFotoPerfil().isEmpty()) {
            holder.getFotoMensajePerfil().setImageResource(R.drawable.icono_launcher_foreground);
        } else {
            Glide.with(c).load(listMensaje.get(position).getFotoPerfil()).into(holder.getFotoMensajePerfil());
        }
        Toast.makeText(holder.itemView.getContext(), "idcurrent", Toast.LENGTH_LONG).show();
        //  return prettyTime.format(date);
        Long codigoHora = listMensaje.get(position).getHora();
        final Date date = new Date(codigoHora);
        PrettyTime prettyTime = new PrettyTime(new Date(), Locale.getDefault());
        final SimpleDateFormat sdf = new SimpleDateFormat("hh:mm:ss a");//a pm o am
        final int finalPosition = position;
        holder.getHora().setText(prettyTime.format(date));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog dialog = new AlertDialog.Builder(view.getContext()).create();
                TextView textView = new TextView(view.getContext());
                dialog.setTitle("Hora");
                dialog.setIcon(R.drawable.img_logo);
                dialog.setView(textView);
                textView.setText(sdf.format(date));
                if (listMensaje.get(finalPosition).getNombre().equals(currentUser.getEmail())) {
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Eliminar mensaje", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            DatabaseReference dR = FirebaseDatabase.getInstance().getReference();
                            dR.child("Chat").child(currentUser.getUid()).child(listMensaje.get(finalPosition).getKeyEmisor()).child(listMensaje.get(finalPosition).getIdmensaje()).setValue(null);
                            listMensaje.remove(finalPosition);
                        }
                    });
                }
                dialog.show();
                //removing artist
            }
        });
    }
    @Override
    public int getItemCount() {
        return listMensaje.size();
    }
    public int getItemViewType(int position) {
        //return super.getItemViewType(position);
        if (listMensaje.get(position).getNombre() != null) {
            if (listMensaje.get(position).getNombre().equals(currentUser.getEmail())) {
                return 1;
            } else {
                return -1;
            }
        } else {
            return -1;
        }
    }
}
