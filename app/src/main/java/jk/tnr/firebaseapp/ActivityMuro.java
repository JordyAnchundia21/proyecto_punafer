package jk.tnr.firebaseapp;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
public class ActivityMuro extends AppCompatActivity {
    private static final int REQUESCODE = 1;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final Map<String, Object> dat = new HashMap<>();
    String IDUSUARIO;
    DatabaseReference databaseSearch = FirebaseDatabase.getInstance().getReference();
    FirebaseAuth firebaseAuth;
    String IdAnuncio;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerSearch;
    private ArrayList<ModelMuro> modelmuro;
    private long primerclick;
    private AdapterMuro adapter;
    private String nombre, anuncio;
    private Uri ImgUriReg;
    private String imagen;
    private String email, tiporegistro;
    private FirebaseUser currentUser;
    private int NOTIFICACION_ID;
    private MediaPlayer song;
    private EditText NombreAnuncio;
    private EditText DescripcionAnuncio;
    private EditText CostoAnuncio;
    private EditText DireccionAnuncio;
    private ImageView ImagenAnuncio;
    private Button AceptarAnuncio;
    private DatabaseReference BaseDatos, BaseDatosUsers;
    private StorageReference mStorage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_muro);
        BaseDatos = FirebaseDatabase.getInstance().getReference("Anuncios");
        BaseDatosUsers = FirebaseDatabase.getInstance().getReference("Usuarios");
        mStorage = FirebaseStorage.getInstance().getReference();
        song = MediaPlayer.create(this, R.raw.sound);
        AlertDialog dialog = new AlertDialog.Builder(this).create();
        EditText editText = new EditText(this);
        firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        IDUSUARIO = currentUser.getUid();
        mRecyclerView = findViewById(R.id.RecyclerMuro);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        modelmuro = new ArrayList<>();
        BaseDatos.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelmuro.clear();
                for (DataSnapshot itemSnapshot2 : dataSnapshot.getChildren()) {
                    ModelAnuncio h2 = itemSnapshot2.getValue(ModelAnuncio.class);
                    final ModelAnuncio fh2 = h2;
                    BaseDatosUsers.child(fh2.getIdusuario()).addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                ModelListUsers u = dataSnapshot.getValue(ModelListUsers.class);
                                ModelMuro h3 = new ModelMuro(fh2, u);
                                modelmuro.add(h3);
                            }
                            adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                            mRecyclerView.setAdapter(adapter);
                        }
                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                        }
                    });
                }
                // adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                //                    mRecyclerView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });




   /*     BaseDatos.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
            //    modelmuro.clear();
                if (dataSnapshot.exists()) {


                    for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                        for (DataSnapshot itemSnapshot2 : itemSnapshot1.getChildren()) {
                            ModelAnuncio h2 = itemSnapshot2.getValue(ModelAnuncio.class);
                            final ModelAnuncio fh2 = h2;

                            BaseDatosUsers.child(fh2.getIdusuario()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        ModelListUsers    u = dataSnapshot.getValue(ModelListUsers.class);

                                        ModelMuro h3 = new ModelMuro(fh2, u);
                                        modelmuro.add(h3);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });

                        }
                    }
                   // adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                    //                    mRecyclerView.setAdapter(adapter);
                }adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                mRecyclerView.setAdapter(adapter);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });*/
   /*     BaseDatos.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        //        modelmuro.clear();
                if (dataSnapshot.exists()) {
                    for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                        for (DataSnapshot itemSnapshot2 : itemSnapshot1.getChildren()) {
                            ModelAnuncio h2 = itemSnapshot2.getValue(ModelAnuncio.class);
                            final ModelAnuncio fh2 = h2;
                            BaseDatosUsers.child(fh2.getIdusuario()).addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                    ModelListUsers u = dataSnapshot.getValue(ModelListUsers.class);
                                    ModelMuro h3 = new ModelMuro(fh2, u);
//                                      modelmuro.add(h3);
                                }
                                @Override
                                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                }
                                @Override
                                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
                                }
                                @Override
                                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                        }
                        adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                        mRecyclerView.setAdapter(adapter);
                    }}
                }
                @Override public void onChildChanged (@NonNull DataSnapshot
                dataSnapshot, @Nullable String s){
                }
                @Override public void onChildRemoved (@NonNull DataSnapshot dataSnapshot){
                }
                @Override public void onChildMoved (@NonNull DataSnapshot
                dataSnapshot, @Nullable String s){
                }
                @Override public void onCancelled (@NonNull DatabaseError databaseError){
                }
            });*/
        FloatingActionButton Search = findViewById(R.id.search);
        mRecyclerSearch = findViewById(R.id.RecyclerSearch);
        mRecyclerSearch.setLayoutManager(new LinearLayoutManager(this));
        Search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final EditText Busqueda = findViewById(R.id.busqueda);
                final String searchText = Busqueda.getText().toString();
                mRecyclerSearch.setVisibility(View.VISIBLE);
                databaseSearch.child("Anuncios").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelmuro.clear();
                        if (dataSnapshot.exists()) {
                            for (DataSnapshot itemSnapshot2 : dataSnapshot.getChildren()) {
                                ModelAnuncio h2 = itemSnapshot2.getValue(ModelAnuncio.class);
                                final ModelAnuncio fh2 = h2;
                                BaseDatosUsers.child(fh2.getIdusuario()).addValueEventListener(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            ModelListUsers u = dataSnapshot.getValue(ModelListUsers.class);
                                            ModelMuro h3 = new ModelMuro(fh2, u);
                                            if (h3.modelAnuncio.getDescripcionanuncio().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h3.modelAnuncio.getDireccionanuncio().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h3.modelAnuncio.getNombreanuncio().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h3.modelAnuncio.getCostoanuncio().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h3.modelListUsers.getNombre().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h3.modelListUsers.getTiporegistro().toLowerCase().contains(Busqueda.getText().toString().toLowerCase())) {
                                                modelmuro.add(h3);
                                            }
                                            adapter = new AdapterMuro(ActivityMuro.this, modelmuro);
                                            mRecyclerSearch.setAdapter(adapter);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {
                                    }
                                });
                            }
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });
        FloatingActionButton anuncioP = findViewById(R.id.anuncioEdit);
        anuncioP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (IDUSUARIO == null) {
                    Toast.makeText(ActivityMuro.this, "Inicie sesión primero", Toast.LENGTH_LONG).show();
                    IDUSUARIO = " ";
                }
                final ProgressDialog pd;
                pd = new ProgressDialog(ActivityMuro.this);
                pd.setMessage("Uploading....");
                pd.setIcon(R.drawable.img_logo);
                pd.setProgressStyle(R.drawable.progress_bar_style);
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(ActivityMuro.this);
                LayoutInflater inflater = getLayoutInflater();
                final View dialogView = inflater.inflate(R.layout.cardview_anuncio, null);
                NombreAnuncio = dialogView.findViewById(R.id.anuncionombre);
                DescripcionAnuncio = dialogView.findViewById(R.id.anunciodescripcion);
                CostoAnuncio = dialogView.findViewById(R.id.anunciocosto);
                DireccionAnuncio = dialogView.findViewById(R.id.anunciodireccion);
                ImagenAnuncio = dialogView.findViewById(R.id.anuncioimagen);
                AceptarAnuncio = dialogView.findViewById(R.id.anunciobotonaceptar);
                dialogBuilder.setView(dialogView);
                final AlertDialog b = dialogBuilder.create();
                b.show();
                ImagenAnuncio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        song.start();
                        openGallery();
                    }
                });
                AceptarAnuncio.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        pd.show();
                        if (ImgUriReg != null) {
                            final String idImg = System.currentTimeMillis() + getImageExt(ImgUriReg);
                            final StorageReference ref = mStorage.child("ImgAnuncio/" + idImg);
                            ref.putFile(ImgUriReg).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                @Override
                                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                    ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                        @Override
                                        public void onSuccess(Uri uri) {
                                            Glide.with(ActivityMuro.this).load(uri).into(ImagenAnuncio);
                                            String name = NombreAnuncio.getText().toString().trim();
                                            String descrip = DescripcionAnuncio.getText().toString().trim();
                                            String costo = CostoAnuncio.getText().toString().trim();
                                            String direc = DireccionAnuncio.getText().toString().trim();
                                            IdAnuncio = databaseReference.child(IDUSUARIO).push().getKey();
                                            dat.put("idanuncio", IdAnuncio);
                                            dat.put("idusuario", currentUser.getUid());
                                            dat.put("nombreanuncio", name);
                                            dat.put("idimagenanuncio", descrip);
                                            dat.put("costoanuncio", costo);
                                            dat.put("descripcionanuncio", descrip);
                                            dat.put("imagenanuncio", uri.toString());
                                            dat.put("direccionanuncio", direc);
                                            Toast.makeText(ActivityMuro.this, "anuncio creado", Toast.LENGTH_LONG).show();
                                            BaseDatos.child(IdAnuncio).updateChildren(dat);
                                            pd.dismiss();
                                            b.dismiss();
                                            ImagenAnuncio.setImageURI(null);
                                        }
                                    });
                                }
                            });
                        } else {
                            String name = NombreAnuncio.getText().toString().trim();
                            String descrip = DescripcionAnuncio.getText().toString().trim();
                            String costo = CostoAnuncio.getText().toString().trim();
                            String direc = DireccionAnuncio.getText().toString().trim();
                            IdAnuncio = databaseReference.child(IDUSUARIO).push().getKey();
                            dat.put("idanuncio", IdAnuncio);
                            dat.put("idusuario", currentUser.getUid());
                            dat.put("nombreanuncio", name);
                            dat.put("idimagenanuncio", descrip);
                            dat.put("costoanuncio", costo);
                            dat.put("descripcionanuncio", descrip);
                            dat.put("direccionanuncio", direc);
                            Toast.makeText(ActivityMuro.this, "anuncio creado", Toast.LENGTH_LONG).show();
                            BaseDatos.child(IdAnuncio).updateChildren(dat);
                            pd.dismiss();
                            b.dismiss();
                        }
                    }
                });
            }
        });
    }
    private String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESCODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {
            ImgUriReg = data.getData();
            ImagenAnuncio.setImageURI(ImgUriReg);
        }
    }
    private void crearNotificacion() {
        String CHANNEL_ID = "Notificacion";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.icono_launcher_foreground);
        builder.setContentTitle("Punafer");
        builder.setContentText("hola");
        builder.setTicker("Nueva Notificacion");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setAutoCancel(true);
        song.start();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());
        //   builder.setSmallIcon(Color.BLUE);
        //  builder.setColor(Color.BLUE);
        //    builder.setVibrate(new long[]{500, 1000, 500, 1000, 500});
        //   builder.setDefaults(Notification.DEFAULT_SOUND);
    }
    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        //  return;
        startActivity(new Intent(ActivityMuro.this, ActivityPrincipal.class));
    }
}

/*  long presionado = 2000;
        if (primerclick + presionado > System.currentTimeMillis()) {
           } else {
            mRecyclerSearch.setVisibility(View.GONE);
        }
        primerclick = System.currentTimeMillis();


        dialog.setTitle("Editar Anuncio");
                dialog.setView(dialogView);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        //editText.setText(anuncio);
                        anuncio = editText.getText().toString();
                        dat.put("anuncio", anuncio);
                        enviarBaseDato();
                    }
                });
                editText.setText(anuncio);
                dialog.show();*/