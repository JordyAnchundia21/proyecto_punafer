package jk.tnr.firebaseapp;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
public class ActivityListUsers extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final DatabaseReference databaseSearch = FirebaseDatabase.getInstance().getReference();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    private ArrayList<ModelListUsers> modelListUsers;
    private AdapterListUsers adapterListUsers;
    private RecyclerView mRecyclerView;
    private RecyclerView mRecyclerSearch;
    private EditText Busqueda;
    private LinearLayout clin;
    private LinearLayout pron;
    private LinearLayout empn;
    private LinearLayout genn;
    private LinearLayout linsearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_users);
        BottomNavigationView bottomNavigationView = findViewById(R.id.bottom_navigation_view);
        FloatingActionButton search = findViewById(R.id.search);
        Busqueda = findViewById(R.id.busqueda);
        String searchText = Busqueda.getText().toString();
        MediaPlayer song = MediaPlayer.create(this, R.raw.sound);
        clin = findViewById(R.id.cli);
        pron = findViewById(R.id.pro);
        empn = findViewById(R.id.emp);
        genn = findViewById(R.id.gen);
        linsearch = findViewById(R.id.searchlin);
        clin.setVisibility(View.GONE);
        empn.setVisibility(View.GONE);
        linsearch.setVisibility(View.GONE);
        pron.setVisibility(View.GONE);
        genn.setVisibility(View.VISIBLE);
        mRecyclerView = findViewById(R.id.myRecyclerG);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        modelListUsers = new ArrayList<>();
        databaseReference.child("Usuarios").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                modelListUsers.clear();
                for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                    ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                    modelListUsers.add(h);
                }
                adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                mRecyclerView.setAdapter(adapterListUsers);
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                linsearch.setVisibility(View.VISIBLE);
                clin.setVisibility(View.GONE);
                empn.setVisibility(View.GONE);
                genn.setVisibility(View.GONE);
                pron.setVisibility(View.GONE);
                mRecyclerSearch = findViewById(R.id.RecyclerSearch);
                mRecyclerSearch.setLayoutManager(new LinearLayoutManager(ActivityListUsers.this));
                modelListUsers = new ArrayList<>();
                databaseSearch.child("Usuarios").addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        modelListUsers.clear();
                        for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                            ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                            if (h.getEmail().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h.getNombre().toLowerCase().contains(Busqueda.getText().toString().toLowerCase()) || h.getTiporegistro().toLowerCase().contains(Busqueda.getText().toString().toLowerCase())) {
                                modelListUsers.add(h);
                            }
                        }
                        adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                        mRecyclerSearch.setAdapter(adapterListUsers);
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
            }
        });
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.muro_users:
                        genn.setVisibility(View.VISIBLE);
                        clin.setVisibility(View.GONE);
                        empn.setVisibility(View.GONE);
                        linsearch.setVisibility(View.GONE);
                        pron.setVisibility(View.GONE);
                        mRecyclerView = findViewById(R.id.myRecyclerG);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ActivityListUsers.this));
                        modelListUsers = new ArrayList<>();
                        databaseReference.child("Usuarios").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                modelListUsers.clear();
                                for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                                    ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                                    modelListUsers.add(h);
                                }
                                adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                                mRecyclerView.setAdapter(adapterListUsers);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                        break;
                    case R.id.muro_client:
                        clin.setVisibility(View.VISIBLE);
                        empn.setVisibility(View.GONE);
                        genn.setVisibility(View.GONE);
                        pron.setVisibility(View.GONE);
                        linsearch.setVisibility(View.GONE);
                        mRecyclerView = findViewById(R.id.myRecyclerC);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ActivityListUsers.this));
                        modelListUsers = new ArrayList<>();
                        databaseReference.child("Usuarios").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                modelListUsers.clear();
                                for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                                    ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                                    if (h.getTiporegistro().equals("Cliente")) {
                                        modelListUsers.add(h);
                                    }
                                }
                                adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                                mRecyclerView.setAdapter(adapterListUsers);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                        break;
                    case R.id.muro_norm:
                        empn.setVisibility(View.VISIBLE);
                        clin.setVisibility(View.GONE);
                        genn.setVisibility(View.GONE);
                        linsearch.setVisibility(View.GONE);
                        pron.setVisibility(View.GONE);
                        mRecyclerView = findViewById(R.id.myRecyclerE);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ActivityListUsers.this));
                        modelListUsers = new ArrayList<>();
                        databaseReference.child("Usuarios").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                modelListUsers.clear();
                                for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                                    ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                                    if (h.getTiporegistro().equals("Empresa")) {
                                        modelListUsers.add(h);
                                    }
                                }
                                adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                                mRecyclerView.setAdapter(adapterListUsers);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                        break;
                    case R.id.muro_prog:
                        pron.setVisibility(View.VISIBLE);
                        clin.setVisibility(View.GONE);
                        empn.setVisibility(View.GONE);
                        genn.setVisibility(View.GONE);
                        linsearch.setVisibility(View.GONE);
                        mRecyclerView = findViewById(R.id.myRecyclerP);
                        mRecyclerView.setLayoutManager(new LinearLayoutManager(ActivityListUsers.this));
                        modelListUsers = new ArrayList<>();
                        databaseReference.child("Usuarios").addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                modelListUsers.clear();
                                for (DataSnapshot itemSnapshot1 : dataSnapshot.getChildren()) {
                                    ModelListUsers h = itemSnapshot1.getValue(ModelListUsers.class);
                                    if (h.getTiporegistro().equals("Programador")) {
                                        modelListUsers.add(h);
                                    }
                                }
                                adapterListUsers = new AdapterListUsers(ActivityListUsers.this, modelListUsers);
                                mRecyclerView.setAdapter(adapterListUsers);
                            }
                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {
                            }
                        });
                }
                return true;
            }
        });
    }
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(ActivityListUsers.this, ActivityPrincipal.class));
    }
}
