package jk.tnr.firebaseapp;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.bumptech.glide.Glide;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
public class ActivityRegister extends AppCompatActivity {
    private static final int REQUESCODE = 1;
    private final Map<String, Object> dat = new HashMap<>();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    String email, password, password2, name, tiporegistro;
    String CHANNEL_ID = "Notificacion";
    private CircleImageView ImgUserPhoto;
    private Uri ImgUriReg;
    private EditText userEmail, userPassword, userPassword2, userName;
    private DatabaseReference BaseDatos;
    private ProgressDialog pd;
    private int NOTIFICACION_ID;
    private StorageReference mStorage;
    private Spinner spinnertip;
    private String mensaje;
    private MediaPlayer song;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        userEmail = findViewById(R.id.regemail);
        userPassword = findViewById(R.id.regpassword);
        userPassword2 = findViewById(R.id.regpassword2);
        userName = findViewById(R.id.regName);
        spinnertip = findViewById(R.id.spinnerTipRegist);
        song = MediaPlayer.create(this, R.raw.sound);
        BaseDatos = FirebaseDatabase.getInstance().getReference("Usuarios");
        mStorage = FirebaseStorage.getInstance().getReference("Foto_Perfil");
        pd = new ProgressDialog(this);
        pd.setMessage("Uploading....");
        pd.setIcon(R.drawable.img_logo);
        pd.setProgressStyle(R.drawable.progress_bar_style);
        ImgUserPhoto = findViewById(R.id.regUserPhoto);
        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                song.start();
                openGallery();
            }
        });
        Button regBtn = findViewById(R.id.registroBtn);
        regBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                song.start();
                email = userEmail.getText().toString();
                password = userPassword.getText().toString();
                password2 = userPassword2.getText().toString();
                name = userName.getText().toString();
                tiporegistro = spinnertip.getSelectedItem().toString();
                if (ImgUriReg == null || tiporegistro.isEmpty() || email.isEmpty() || name.isEmpty() || password.isEmpty() || !password.equals(password2)) {
                    Toast.makeText(ActivityRegister.this, "Verifique que no existan campos vacios", Toast.LENGTH_LONG).show();
                    song.start();
                } else {
                    pd.show();
                    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    if (firebaseUser == null) {
                        CreateUserAccount(email, password, name, tiporegistro);
                    } else {
                        FirebaseAuth.getInstance().signOut();
                        CreateUserAccount(email, password, name, tiporegistro);
                    }
                }
            }
        });
    }
    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESCODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        pd.show();
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {
            ImgUriReg = data.getData();
            ImgUserPhoto.setImageURI(ImgUriReg);
            pd.dismiss();
        }
    }
    private String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    private void CreateUserAccount(final String email, final String password, final String name, final String tiporegistro) {
        final String idImg = System.currentTimeMillis() + getImageExt(ImgUriReg);
        final StorageReference ref = mStorage.child(idImg);
        final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        firebaseAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(ActivityRegister.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ActivityRegister.this, "Se ha registrado el usuario con el" + " email: " + userEmail.getText(), Toast.LENGTH_LONG).show();
                    ref.putFile(ImgUriReg).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(final Uri uri) {
                                    UserProfileChangeRequest profleUpdate = new UserProfileChangeRequest.Builder().setDisplayName(name).setPhotoUri(uri).build();
                                    final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
                                    currentUser.updateProfile(profleUpdate).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Toast.makeText(ActivityRegister.this, "Update inform correcto", Toast.LENGTH_LONG).show();
                                                String idusuario = currentUser.getUid();
                                                switch (tiporegistro) {
                                                    case "Cliente":
                                                        dat.put("nombre", name);
                                                        dat.put("imagen", uri.toString());
                                                        dat.put("idimagen", idImg);
                                                        dat.put("email", email);
                                                        dat.put("password", password);
                                                        dat.put("tiporegistro", tiporegistro);
                                                        dat.put("idusuario", idusuario);
                                                        dat.put("status", " ");
                                                        dat.put("celular", " ");
                                                        BaseDatos.child(idusuario).updateChildren(dat);
                                                        pd.dismiss();
                                                        Toast.makeText(ActivityRegister.this, "Base Dato creada", Toast.LENGTH_LONG).show();
                                                        startActivity(new Intent(ActivityRegister.this, ActivityProfile.class));
                                                        finish();
                                                        break;
                                                    case "Empresa":
                                                        dat.put("nombre", name);
                                                        dat.put("imagen", uri.toString());
                                                        dat.put("idimagen", idImg);
                                                        dat.put("email", email);
                                                        dat.put("password", password);
                                                        dat.put("tiporegistro", tiporegistro);
                                                        dat.put("idusuario", idusuario);
                                                        dat.put("status", " ");
                                                        dat.put("celular", " ");
                                                        dat.put("dirección", " ");
                                                        BaseDatos.child(idusuario).updateChildren(dat);
                                                        pd.dismiss();
                                                        Toast.makeText(ActivityRegister.this, "Base Dato creada", Toast.LENGTH_LONG).show();
                                                        startActivity(new Intent(ActivityRegister.this, ActivityProfile.class));
                                                        finish();
                                                        break;
                                                    case "Programador":
                                                        dat.put("nombre", name);
                                                        dat.put("imagen", uri.toString());
                                                        dat.put("idimagen", idImg);
                                                        dat.put("email", email);
                                                        dat.put("password", password);
                                                        dat.put("tiporegistro", tiporegistro);
                                                        dat.put("idusuario", idusuario);
                                                        dat.put("status", " ");
                                                        dat.put("celular", " ");
                                                        dat.put("recomendaciones", " ");
                                                        dat.put("lenguaje", " ");
                                                        dat.put("experiencia", " ");
                                                        dat.put("dirección", " ");
                                                        BaseDatos.child(idusuario).updateChildren(dat);
                                                        pd.dismiss();
                                                        Toast.makeText(ActivityRegister.this, "Base Dato creada", Toast.LENGTH_LONG).show();
                                                        startActivity(new Intent(ActivityRegister.this, ActivityProfile.class));
                                                        finish();
                                                        break;
                                                }
                                                Glide.with(ActivityRegister.this).load(uri).into(ImgUserPhoto);
                                            } else {
                                                pd.dismiss();
                                                Toast.makeText(ActivityRegister.this, "Update inform fallido", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    });
                } else {
                    pd.dismiss();
                    Toast.makeText(ActivityRegister.this, "Registro Fallido ", Toast.LENGTH_LONG).show();
                }
            }
        });
    }
    public void onBackPressed() {
        //        super.onBackPressed();
        startActivity(new Intent(ActivityRegister.this, ActivityPrincipal.class));
    }
    public void createNotificacionChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notification";
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
    private void crearNotificacion() {
        String CHANNEL_ID = "Notificacion";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(ActivityRegister.this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.icono_launcher_foreground);
        builder.setContentTitle("Punafer");
        builder.setContentText(mensaje);
        builder.setTicker("Nueva Notificación");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setAutoCancel(true);
        song.start();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(ActivityRegister.this);
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());
        //   builder.setSmallIcon(Color.BLUE);
        //  builder.setColor(Color.BLUE);
        //    builder.setVibrate(new long[]{500, 1000, 500, 1000, 500});
        //   builder.setDefaults(Notification.DEFAULT_SOUND);
    }
    private void guardarDatos() {
    }
}