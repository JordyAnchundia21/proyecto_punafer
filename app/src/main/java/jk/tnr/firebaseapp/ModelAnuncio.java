package jk.tnr.firebaseapp;
class ModelAnuncio {
    private String idusuario;
    private String idanuncio;
    private String nombreanuncio;
    private String idimagenanuncio;
    private String costoanuncio;
    private String descripcionanuncio;
    private String imagenanuncio;
    private String direccionanuncio;
    public ModelAnuncio() {
    }
    public ModelAnuncio(String idusuario, String idanuncio, String nombreanuncio, String idimagenanuncio, String costoanuncio, String descripcionanuncio, String imagenanuncio, String direccionanuncio) {
        this.idusuario = idusuario;
        this.idanuncio = idanuncio;
        this.nombreanuncio = nombreanuncio;
        this.idimagenanuncio = idimagenanuncio;
        this.costoanuncio = costoanuncio;
        this.descripcionanuncio = descripcionanuncio;
        this.imagenanuncio = imagenanuncio;
        this.direccionanuncio = direccionanuncio;
    }
    public String getIdusuario() {
        return idusuario;
    }
    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }
    public String getIdanuncio() {
        return idanuncio;
    }
    public void setIdanuncio(String idanuncio) {
        this.idanuncio = idanuncio;
    }
    public String getNombreanuncio() {
        return nombreanuncio;
    }
    public void setNombreanuncio(String nombreanuncio) {
        this.nombreanuncio = nombreanuncio;
    }
    public String getIdimagenanuncio() {
        return idimagenanuncio;
    }
    public void setIdimagenanuncio(String idimagenanuncio) {
        this.idimagenanuncio = idimagenanuncio;
    }
    public String getCostoanuncio() {
        return costoanuncio;
    }
    public void setCostoanuncio(String costoanuncio) {
        this.costoanuncio = costoanuncio;
    }
    public String getDescripcionanuncio() {
        return descripcionanuncio;
    }
    public void setDescripcionanuncio(String descripcionanuncio) {
        this.descripcionanuncio = descripcionanuncio;
    }
    public String getImagenanuncio() {
        return imagenanuncio;
    }
    public void setImagenanuncio(String imagenanuncio) {
        this.imagenanuncio = imagenanuncio;
    }
    public String getDireccionanuncio() {
        return direccionanuncio;
    }
    public void setDireccionanuncio(String direccionanuncio) {
        this.direccionanuncio = direccionanuncio;
    }
}
