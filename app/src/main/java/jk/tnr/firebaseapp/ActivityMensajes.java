package jk.tnr.firebaseapp;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

import de.hdodenhof.circleimageview.CircleImageView;
public class ActivityMensajes extends AppCompatActivity {
    private static final int PHOTO_SEND = 1;
    private static final int PHOTO_PERFIL = 2;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    ModelMensajeEnviar emisor;
    ModelMensajeEnviar receptor;
    String CHANNEL_ID = "Notificacion";
    private RecyclerView rvMensajes;
    private EditText txtMensaje;
    private AdapterMensajes adapter;
    private FirebaseDatabase database;
    private ArrayList<ModelMuro> modelmuro;
    private FirebaseStorage storage;
    private int NOTIFICACION_ID;
    private StorageReference storageReference;
    private String fotoPerfilCadena;
    private MediaPlayer song;
    private String nombreCurrent;
    private String emailCurrent;
    private String imagenCurrent;
    private String idBundle;
    private FirebaseUser currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mensaje);
        song = MediaPlayer.create(this, R.raw.sound);
        CircleImageView fotoPerfil = findViewById(R.id.fotoPerfil);
        TextView nombre = findViewById(R.id.nombre);
        final TextView status = findViewById(R.id.status);
        rvMensajes = findViewById(R.id.rvMensajes);
        txtMensaje = findViewById(R.id.txtMensaje);
        FloatingActionButton btnEnviar = findViewById(R.id.btnEnviar);
        ImageButton btnEnviarFoto = findViewById(R.id.btnEnviarFoto);
        fotoPerfilCadena = "";
        Bundle bundle = this.getIntent().getExtras();
        String nombreBundle;
        if (bundle != null) {
            idBundle = bundle.getString("id");
            String imagenBundle = bundle.getString("imagen");
            String emailBundle = bundle.getString("email");
            nombreBundle = bundle.getString("nombre");
            nombre.setText(nombreBundle);
            Picasso.get().load(imagenBundle).into(fotoPerfil);
        } else {
            nombreBundle = "";
        }
        databaseReference.child("Usuarios").child(idBundle).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //String userType = dataSnapshot.child("tiporegistro").getValue().toString();
                if (dataSnapshot.exists()) {
                    String estatus = dataSnapshot.child("status").getValue().toString();
                    status.setText(estatus);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        storage = FirebaseStorage.getInstance();
        adapter = new AdapterMensajes(this);
        LinearLayoutManager l = new LinearLayoutManager(this);
        rvMensajes.setLayoutManager(l);
        rvMensajes.setAdapter(adapter);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        StorageReference storageRef = storage.getReference("fotoPerfil");
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        currentUser = firebaseAuth.getCurrentUser();
        databaseReference.child("Usuarios").child(IDUSUARIO).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //String userType = dataSnapshot.child("tiporegistro").getValue().toString();
                if (dataSnapshot.exists()) {
                    nombreCurrent = dataSnapshot.child("nombre").getValue().toString();
                    emailCurrent = dataSnapshot.child("email").getValue().toString();
                    imagenCurrent = dataSnapshot.child("imagen").getValue().toString();
                    //Glide.with(this).load(imagen).into(ImgUserPhoto);
                    fotoPerfilCadena = imagenCurrent;
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
        btnEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (txtMensaje != null) {
                    song.start();
                    String IdMensajeEmisor = databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).push().getKey();
                    String IdMensajeReceptor = databaseReference.child("Chat").child(idBundle).child(IDUSUARIO).push().getKey();
                    databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).child(IdMensajeEmisor).setValue(new ModelMensajeEnviar(txtMensaje.getText().toString(), emailCurrent, fotoPerfilCadena, "1", ServerValue.TIMESTAMP, IdMensajeEmisor, currentUser.getUid()));
                    databaseReference.child("Chat").child(idBundle).child(IDUSUARIO).child(IdMensajeReceptor).setValue(new ModelMensajeEnviar(txtMensaje.getText().toString(), emailCurrent, fotoPerfilCadena, "1", ServerValue.TIMESTAMP, IdMensajeReceptor, currentUser.getUid()));
                    txtMensaje.setText("");
                }
            }
        });
        btnEnviarFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                song.start();
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/*");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), PHOTO_SEND);
            }
        });
    /*    fotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                song.start();
                Intent i = new Intent(Intent.ACTION_GET_CONTENT);
                i.setType("image/jpeg");
                i.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(Intent.createChooser(i, "Selecciona una foto"), PHOTO_PERFIL);
            }
        });*/
        adapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                setScrollbar();
            }
        });
        databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ModelMensajeRecibir m = dataSnapshot.getValue(ModelMensajeRecibir.class);
                adapter.addMensaje(m);
            }
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                createNotificacionChannel();
                String mensaje = "Nuevo mensaje";
                crearNotificacion(mensaje);
            }
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }
            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        /*       databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //String userType = dataSnapshot.child("tiporegistro").getValue().toString();
                if (dataSnapshot.exists()) {
                    ModelMensajeRecibir m = dataSnapshot.getValue(ModelMensajeRecibir.class);
                    adapter.addMensaje(m);
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });*/
    }
    private void setScrollbar() {
        rvMensajes.scrollToPosition(adapter.getItemCount() - 1);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PHOTO_SEND && resultCode == RESULT_OK) {
            Uri u = data.getData();
            final StorageReference fotoReferencia = storage.getReference("fotoPerfiles_chat").child(u.getLastPathSegment());
            fotoReferencia.putFile(u).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    fotoReferencia.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String IdMensajeEmisor = databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).push().getKey();
                            String IdMensajeReceptor = databaseReference.child("Chat").child(idBundle).child(IDUSUARIO).push().getKey();
                            databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).child(IdMensajeEmisor).
                                    setValue(new ModelMensajeEnviar("has enviado una foto", uri.toString(), emailCurrent, fotoPerfilCadena, "2", ServerValue.TIMESTAMP, IdMensajeEmisor, currentUser.getUid()));
                            databaseReference.child("Chat").child(idBundle).child(IDUSUARIO).child(IdMensajeReceptor).
                                    setValue(new ModelMensajeEnviar(nombreCurrent + "ha enviado una foto", uri.toString(), emailCurrent, fotoPerfilCadena, "2", ServerValue.TIMESTAMP, IdMensajeEmisor, currentUser.getUid()));
                        }
                    });
                }
            });
        }
    }
    /*else if (requestCode == PHOTO_PERFIL && resultCode == RESULT_OK) {
                Uri u = data.getData();
                storageReference = storage.getReference("foto_perfil");//imagenes_chat
                final StorageReference fotoReferencia = storageReference.child(u.getLastPathSegment());
                fotoReferencia.putFile(u).addOnSuccessListener(this, new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        Uri u = taskSnapshot.getUploadSessionUri();
                        //  Uri u = taskSnapshot.getDownloadUrl();
                        fotoPerfilCadena = u.toString();
                        receptor= new ModelMensajeEnviar("Kevin ha actualizado su foto de perfil", u.toString(), emailCurrent, fotoPerfilCadena, "2", ServerValue.TIMESTAMP);
                        receptor= new ModelMensajeEnviar("Kevin ha actualizado su foto de perfil", u.toString(), emailCurrent, fotoPerfilCadena, "2", ServerValue.TIMESTAMP);
                        enviar(emisor, receptor);
                        Glide.with(ActivityMensajes.this).load(u.toString()).into(fotoPerfil);
                    }
                });
            }*/
    public void createNotificacionChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Notification";
            NotificationChannel notificationChannel = new NotificationChannel(CHANNEL_ID, name, NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(notificationChannel);
        }
    }
    private void crearNotificacion(String mensaje) {
        String CHANNEL_ID = "Notificacion";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.icono_launcher_foreground);
        builder.setContentTitle("Punafer");
        builder.setContentText(mensaje);
        builder.setTicker("Nueva Notificación");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setAutoCancel(true);
        song.start();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());
        //   builder.setSmallIcon(Color.BLUE);
        //  builder.setColor(Color.BLUE);
        //    builder.setVibrate(new long[]{500, 1000, 500, 1000, 500});
        //   builder.setDefaults(Notification.DEFAULT_SOUND);
    }
    public void enviar(ModelMensajeEnviar emisor, ModelMensajeEnviar receptor) {
        databaseReference.child("Chat").child(IDUSUARIO).child(idBundle).push().setValue(emisor);
        databaseReference.child("Chat").child(idBundle).child(IDUSUARIO).push().setValue(receptor);
    }
    @Override
    protected void onResume() {
        super.onResume();
        status("online");
    }
    @Override
    protected void onPause() {
        super.onPause();
        status("offline");
    }
    private void status(String status) {
        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("Usuarios").child(IDUSUARIO);
        HashMap<String, Object> dat = new HashMap<>();
        dat.put("status", status);
        reference.updateChildren(dat);
    }
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(ActivityMensajes.this, ActivityPrincipal.class));
    }
}