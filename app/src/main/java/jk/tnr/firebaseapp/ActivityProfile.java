package jk.tnr.firebaseapp;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
public class ActivityProfile extends AppCompatActivity {
    private static final int REQUESCODE = 1;
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    private final Map<String, Object> dat = new HashMap<>();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    private Uri ImgUriReg;
    private EditText editText;
    private StorageReference mStorage;
    private AlertDialog dialog;
    private ProgressDialog pd;
    private TextView NombreProfil, CelularProfil, EmailProfil, TipoRegistroProfil, RecomendacionesProfil, ExperienciaProfil, LenguajeProfil, DireccionProfil;
    private CircleImageView ImgUserPhoto;
    private String nombre, celular, imagen, idimagen, email, tiporegistro, lenguaje, experiencia, recomendaciones, direccion;
    private Button actualizarImagenPhotoPerfil;
    private LinearLayout LinDir, LinExp, LinRec, LinLeng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("16B879AF647EB644CC229CEA3CC82FC2").build();
        mAdView.loadAd(adRequest);
        final MediaPlayer song = MediaPlayer.create(this, R.raw.sound);
        mStorage = FirebaseStorage.getInstance().getReference("Foto_Perfil");
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        NombreProfil = findViewById(R.id.nombreProfilUser);
        EmailProfil = findViewById(R.id.emailProfilUser);
        mStorage = FirebaseStorage.getInstance().getReference();
        DireccionProfil = findViewById(R.id.direccionProfilUser);
        CelularProfil = findViewById(R.id.celularProfilUser);
        TipoRegistroProfil = findViewById(R.id.tiporegistroProfilUser);
        ImgUserPhoto = findViewById(R.id.photoProfilUser);
        actualizarImagenPhotoPerfil = findViewById(R.id.actualizarImagenProfilUser);
        LenguajeProfil = findViewById(R.id.lenguajeProfilUser);
        ExperienciaProfil = findViewById(R.id.experienciaProfilUser);
        RecomendacionesProfil = findViewById(R.id.recomendacionesProfilUser);
        LinDir = findViewById(R.id.linDirec);
        LinExp = findViewById(R.id.linExpe);
        LinRec = findViewById(R.id.linRecomend);
        LinLeng = findViewById(R.id.linLenguaje);
        Toast.makeText(ActivityProfile.this, DatosUsuario.getInstancia().IdUsuario(), Toast.LENGTH_LONG).show();
        if (IDUSUARIO == null) {
            Toast.makeText(ActivityProfile.this, "Usted no ha iniciado sesión", Toast.LENGTH_LONG).show();
            IDUSUARIO = " ";
        }
        dialog = new AlertDialog.Builder(this).create();
        editText = new EditText(this);
        pd = new ProgressDialog(this);
        pd.setMessage("Uploading....");
        ImgUserPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                song.start();
                openGallery();
            }
        });
        NombreProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Nombre");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        NombreProfil.setText(editText.getText());
                        String nombre = NombreProfil.getText().toString();
                        dat.put("nombre", nombre);
                        enviarBaseDato();
                    }
                });
                editText.setText(NombreProfil.getText());
                dialog.show();
            }
        });
        CelularProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Celular");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        CelularProfil.setText(editText.getText());
                        String celular = CelularProfil.getText().toString();
                        dat.put("celular", celular);
                        enviarBaseDato();
                    }
                });
                editText.setText(CelularProfil.getText());
                dialog.show();
            }
        });
        ExperienciaProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Experiencia");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ExperienciaProfil.setText(editText.getText());
                        String experiencia = ExperienciaProfil.getText().toString();
                        dat.put("experiencia", experiencia);
                        enviarBaseDato();
                    }
                });
                editText.setText(ExperienciaProfil.getText());
                dialog.show();
            }
        });
        RecomendacionesProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Recomendaciones");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        RecomendacionesProfil.setText(editText.getText());
                        String recomendaciones = RecomendacionesProfil.getText().toString();
                        dat.put("recomendaciones", recomendaciones);
                        enviarBaseDato();
                    }
                });
                editText.setText(RecomendacionesProfil.getText());
                dialog.show();
            }
        });
        LenguajeProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Lenguaje");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        LenguajeProfil.setText(editText.getText());
                        String lenguaje = LenguajeProfil.getText().toString();
                        dat.put("lenguaje", lenguaje);
                        enviarBaseDato();
                    }
                });
                editText.setText(LenguajeProfil.getText());
                dialog.show();
            }
        });
        DireccionProfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.setTitle("Editar Direccion");
                dialog.setView(editText);
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Guardar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        DireccionProfil.setText(editText.getText());
                        String direccion = DireccionProfil.getText().toString();
                        dat.put("direccion", direccion);
                        enviarBaseDato();
                    }
                });
                editText.setText(DireccionProfil.getText());
                dialog.show();
            }
        });
        actualizarImagenPhotoPerfil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ImgUriReg != null) {
                    //    final String idImg = currentUser.getUid();
                    final StorageReference ref = mStorage.child(idimagen);
                    ref.putFile(ImgUriReg).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                @Override
                                public void onSuccess(Uri uri) {
                                    pd.show();
                                    dat.put("imagen", uri.toString());
                                    //      dat.put("idimagen", idImg);
                                    enviarBaseDato();
                                    Toast.makeText(ActivityProfile.this, "Upload Succesfully", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });
                }
            }
        });
        databaseReference.child("Usuarios").
                child(IDUSUARIO).
                addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        //String userType = dataSnapshot.child("tiporegistro").getValue().toString();
                        if (dataSnapshot.exists()) {
                            if (dataSnapshot.child("nombre").exists()) {
                                nombre = dataSnapshot.child("nombre").getValue().toString();
                                NombreProfil.setText(nombre);
                            } else {
                                NombreProfil.setText("");
                            }
                            if (dataSnapshot.child("celular").exists()) {
                                celular = dataSnapshot.child("celular").getValue().toString();
                                CelularProfil.setText(celular);
                            } else {
                                CelularProfil.setText("");
                            }
                            if (dataSnapshot.child("email").exists()) {
                                email = dataSnapshot.child("email").getValue().toString();
                                EmailProfil.setText(email);
                            } else {
                                EmailProfil.setText("");
                            }
                            if (dataSnapshot.child("imagen").exists()) {
                                imagen = dataSnapshot.child("imagen").getValue().toString();
                                idimagen = dataSnapshot.child("idimagen").getValue().toString();
                                Picasso.get().load(imagen).into(ImgUserPhoto);
                            } else {
                                Picasso.get().load(R.drawable.img_no_disponible).into(ImgUserPhoto);
                            }
                            if (dataSnapshot.child("tiporegistro").exists()) {
                                tiporegistro = dataSnapshot.child("tiporegistro").getValue().toString();
                                TipoRegistroProfil.setText(String.format("Perfil de %s", tiporegistro));
                            } else {
                                tiporegistro = "";
                                TipoRegistroProfil.setText("");
                            }
                            if (dataSnapshot.child("lenguaje").exists()) {
                                lenguaje = dataSnapshot.child("lenguaje").getValue().toString();
                                LenguajeProfil.setText(lenguaje);
                            } else {
                                // LenguajeProfil.setText("");
                                LinLeng.setVisibility(View.GONE);
                            }
                            if (dataSnapshot.child("experiencia").exists()) {
                                experiencia = dataSnapshot.child("experiencia").getValue().toString();
                                ExperienciaProfil.setText(experiencia);
                            } else {
                                //  LenguajeProfil.setText("");
                                LinExp.setVisibility(View.GONE);
                            }
                            if (dataSnapshot.child("recomendaciones").exists()) {
                                recomendaciones = dataSnapshot.child("recomendaciones").getValue().toString();
                                RecomendacionesProfil.setText(recomendaciones);
                            } else {
                                //    RecomendacionesProfil.setText("");
                                LinRec.setVisibility(View.GONE);
                            }
                            if (dataSnapshot.child("direccion").exists()) {
                                direccion = dataSnapshot.child("direccion").getValue().toString();
                                DireccionProfil.setText(direccion);
                            } else {
                                //   DireccionProfil.setText("");
                                LinDir.setVisibility(View.GONE);
                            }
                            //Glide.with(this).load(imagen).into(ImgUserPhoto);
                        } else {
                            Toast.makeText(ActivityProfile.this, "No se encuentra el  perfil", Toast.LENGTH_LONG).show();
                        }
                    }
                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {
                    }
                });
    }
    private String getImageExt(Uri uri) {
        ContentResolver contentResolver = getContentResolver();
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        return mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri));
    }
    private void openGallery() {
        Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent.setType("image/*");
        startActivityForResult(galleryIntent, REQUESCODE);
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == REQUESCODE && data != null) {
            ImgUriReg = data.getData();
            ImgUserPhoto.setImageURI(ImgUriReg);
            actualizarImagenPhotoPerfil.setVisibility(View.VISIBLE);
        }
    }
    private void enviarBaseDato() {
        databaseReference.child("Usuarios").child(IDUSUARIO).updateChildren(dat);
        Toast.makeText(this, "Cambio Guardado", Toast.LENGTH_LONG).show();
    }
    public void onBackPressed() {
        //        super.onBackPressed();
        startActivity(new Intent(ActivityProfile.this, ActivityPrincipal.class));
    }
}
