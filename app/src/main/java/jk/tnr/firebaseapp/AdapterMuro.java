package jk.tnr.firebaseapp;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;
//import com.jsibbold.zoomage.ZoomageView;
public class AdapterMuro extends RecyclerView.Adapter<AdapterMuro.MyViewHolder> {
    private final Context context;
    private final Map<String, Object> dat = new HashMap<>();
    private final ArrayList<ModelMuro> modelmuro;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private final FirebaseUser currentUser = firebaseAuth.getCurrentUser();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    private String imagen;
    private EditText NombreAnuncio;
    private EditText DescripcionAnuncio;
    private EditText CostoAnuncio;
    private EditText DireccionAnuncio;
    private ImageView ImagenAnuncio;
    private Button AceptarAnuncio;
    private ProgressDialog pd;
    AdapterMuro(Context c, ArrayList<ModelMuro> p) {
        context = c;
        modelmuro = p;
    }
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_muro, viewGroup, false));
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        //position = ((getItemCount()) - (position + 1));
        holder.nom.setText(modelmuro.get(position).modelAnuncio.getNombreanuncio());
        holder.desc.setText(modelmuro.get(position).modelAnuncio.getDescripcionanuncio());
        holder.cost.setText(modelmuro.get(position).modelAnuncio.getCostoanuncio());
        holder.direc.setText(modelmuro.get(position).modelAnuncio.getDireccionanuncio());
        Picasso.get().load(modelmuro.get(position).modelAnuncio.getImagenanuncio()).into(holder.imaganuncio);
        holder.nombre.setText(modelmuro.get(position).modelListUsers.getNombre());
        holder.tiporegistro.setText(modelmuro.get(position).modelListUsers.getTiporegistro());
        Picasso.get().load(modelmuro.get(position).modelListUsers.getImagen()).into(holder.image);
        final int finalPosition = position;
        holder.imaganuncio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            /*   AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(view.getContext());
                View dialogView = LayoutInflater.from(context).inflate(R.layout.img_pantalla_completa, null);
                ImageView Imagenp = dialogView.findViewById(R.id.imgShow);
                //      Imagenp.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));
                //       Imagenp.setAdjustViewBounds(true);
                Picasso.get().load(modelmuro.get(finalPosition).modelAnuncio.getImagenanuncio()).into(Imagenp);
                // Imagenp.setImageResource(modelmuro.get(finalPosition).modelAnuncio.getImagenanuncio());
                dialogBuilder.setView(dialogView);
                final AlertDialog b = dialogBuilder.create();
                b.show();*/
                Intent intent = new Intent(view.getContext(), ImagenActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("imagen", modelmuro.get(finalPosition).modelAnuncio.getImagenanuncio());
                // método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });
  /* view = inflater.inflate(R.layout.fragment_image_full_screen, container, false);
            ImageZoomageView = view.findViewById(R.id.imageViewImageFullScreen);
            ImageZoomageView.setImageResource(R.drawable.image);

            if(isImageFitToScreen) {
                    isImageFitToScreen=false;
                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT));

                }else{
                    isImageFitToScreen=true;
                    imageView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                       imageView.setAdjustViewBounds(true);   imageView.setScaleType(ImageView.ScaleType.FIT_XY);
                }*/
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                pd = new ProgressDialog(view.getContext());
                pd.setMessage("Uploading....");
                pd.setIcon(R.drawable.img_logo);
                AlertDialog dialog;
                dialog = new AlertDialog.Builder(view.getContext()).create();
                if (modelmuro.get(finalPosition).modelAnuncio.getIdusuario().equals(currentUser.getUid())) {
                    dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Eliminar anuncio", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pd.setMessage("Eliminando anuncio...");
                            pd.show();
                            DatabaseReference dR = FirebaseDatabase.getInstance().getReference();
                            dR.child("Anuncios").child(modelmuro.get(finalPosition).getModelAnuncio().getIdanuncio()).setValue(null);
                            //    listMensaje.remove(finalPosition);
                            pd.dismiss();
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ver Perfil", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(view.getContext(), ActivityPerfilPublico.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", modelmuro.get(finalPosition).modelAnuncio.getIdusuario());
                            intent.putExtras(bundle);
                            view.getContext().startActivity(intent);
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Editar", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(view.getContext());
                            View dialogView = LayoutInflater.from(context).inflate(R.layout.cardview_anuncio, null);
                            NombreAnuncio = dialogView.findViewById(R.id.anuncionombre);
                            DescripcionAnuncio = dialogView.findViewById(R.id.anunciodescripcion);
                            CostoAnuncio = dialogView.findViewById(R.id.anunciocosto);
                            DireccionAnuncio = dialogView.findViewById(R.id.anunciodireccion);
                            ImagenAnuncio = dialogView.findViewById(R.id.anuncioimagen);
                            AceptarAnuncio = dialogView.findViewById(R.id.anunciobotonaceptar);
                            dialogBuilder.setView(dialogView);
                            final AlertDialog b = dialogBuilder.create();
                            b.show();
                            final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                            databaseReference.child("Anuncios").child(modelmuro.get(finalPosition).getModelAnuncio().getIdanuncio()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    if (dataSnapshot.exists()) {
                                        String nomb = dataSnapshot.child("nombreanuncio").getValue().toString();
                                        NombreAnuncio.setText(nomb);
                                        String des = dataSnapshot.child("descripcionanuncio").getValue().toString();
                                        DescripcionAnuncio.setText(des);
                                        String cos = dataSnapshot.child("costoanuncio").getValue().toString();
                                        CostoAnuncio.setText(cos);
                                        String dir = dataSnapshot.child("direccionanuncio").getValue().toString();
                                        DireccionAnuncio.setText(dir);
                                        imagen = dataSnapshot.child("imagenanuncio").getValue().toString();
                                        Picasso.get().load(imagen).into(ImagenAnuncio);
                                        //Glide.with(this).load(imagen).into(imageBarra);
                                    }
                                }
                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {
                                }
                            });
                            AceptarAnuncio.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    pd.setMessage("actualizando anuncio...");
                                    pd.show();
                                    String name = NombreAnuncio.getText().toString().trim();
                                    String descrip = DescripcionAnuncio.getText().toString().trim();
                                    String costo = CostoAnuncio.getText().toString().trim();
                                    String direc = DireccionAnuncio.getText().toString().trim();
                                    if (!TextUtils.isEmpty(name)) {
                                        dat.put("nombreanuncio", name);
                                        dat.put("costoanuncio", costo);
                                        dat.put("descripcionanuncio", descrip);
                                        //     dat.put("imagenanuncio", uri.toString());
                                        dat.put("direccionanuncio", direc);
                                        Toast.makeText(view.getContext(), "Datos actualizados", Toast.LENGTH_LONG).show();
                                        databaseReference.child("Anuncios").child(modelmuro.get(finalPosition).getModelAnuncio().getIdanuncio()).updateChildren(dat);
                                        pd.dismiss();
                                        b.dismiss();
                                    }
                                }
                            });
                        }
                    });
                    dialog.show();
                } else {
                    dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Ver Perfil", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(view.getContext(), ActivityPerfilPublico.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", modelmuro.get(finalPosition).modelAnuncio.getIdusuario());
                            intent.putExtras(bundle);
                            view.getContext().startActivity(intent);
                        }
                    });
                    dialog.setButton(DialogInterface.BUTTON_NEUTRAL, "Enviar mensaje", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(view.getContext(), ActivityMensajes.class);
                            Bundle bundle = new Bundle();
                            bundle.putString("id", modelmuro.get(finalPosition).modelListUsers.getIdusuario());
                            bundle.putString("nombre", modelmuro.get(finalPosition).modelListUsers.getNombre());
                            bundle.putString("email", modelmuro.get(finalPosition).modelListUsers.getEmail());
                            bundle.putString("imagen", modelmuro.get(finalPosition).modelListUsers.getImagen());
                            // método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                            intent.putExtras(bundle);
                            view.getContext().startActivity(intent);
                        }
                    });
                    dialog.show();
                }
            }
        });
    }
    @Override
    public int getItemCount() {
        return modelmuro.size();
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        final CircleImageView image;
        final TextView nombre;
        final TextView nom;
        final TextView desc;
        final TextView cost;
        final TextView direc;
        final ImageView imaganuncio;
        final TextView tiporegistro;
        final TextView IdUser;
        final LinearLayout Layout;
        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            Layout = itemView.findViewById(R.id.linearmuro);
            image = itemView.findViewById(R.id.imgMuro);
            nombre = itemView.findViewById(R.id.nameMuro);
            tiporegistro = itemView.findViewById(R.id.tipRegMuro);
            IdUser = itemView.findViewById(R.id.iduser);
            nom = itemView.findViewById(R.id.anuncionombre);
            desc = itemView.findViewById(R.id.anunciodescripcion);
            cost = itemView.findViewById(R.id.anunciocosto);
            direc = itemView.findViewById(R.id.anunciodireccion);
            imaganuncio = itemView.findViewById(R.id.anuncioimagen);
         /*   imaganuncio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //      View dialogView = LayoutInflater.from(context).inflate(R.layout.img_pantalla_completa, null);
                    imaganuncio.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                    imaganuncio.setAdjustViewBounds(true);
                }
            });*/
        }
    }
}