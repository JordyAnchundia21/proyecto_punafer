package jk.tnr.firebaseapp;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;
public class ActivityPerfilPublico extends AppCompatActivity {
    private final DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    private TextView NombreProfil, CelularProfil, AnuncioProfil, EmailProfil, TipoRegistroProfil, RecomendacionesProfil, ExperienciaProfil, LenguajeProfil, DireccionProfil;
    private CircleImageView ImgUserPhoto;
    private String nombre, celular, anuncio, imagen, email, tiporegistro, lenguaje, experiencia, recomendaciones, direccion;
    private LinearLayout LinDir, LinExp, LinRec, LinLeng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perfil_publico);

  MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("16B879AF647EB644CC229CEA3CC82FC2").build();
        mAdView.loadAd(adRequest);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
        NombreProfil = findViewById(R.id.nombreProfilUser);
        EmailProfil = findViewById(R.id.emailProfilUser);
        DireccionProfil = findViewById(R.id.direccionProfilUser);
        CelularProfil = findViewById(R.id.celularProfilUser);
        TipoRegistroProfil = findViewById(R.id.tiporegistroProfilUser);
        ImgUserPhoto = findViewById(R.id.photoProfilUser);
        LenguajeProfil = findViewById(R.id.lenguajeProfilUser);
        ExperienciaProfil = findViewById(R.id.experienciaProfilUser);
        RecomendacionesProfil = findViewById(R.id.recomendacionesProfilUser);
        LinDir = findViewById(R.id.linDirec);
        LinExp = findViewById(R.id.linExpe);
        LinRec = findViewById(R.id.linRecomend);
        LinLeng = findViewById(R.id.linLenguaje);
        ProgressDialog pd = new ProgressDialog(this);
        pd.setMessage("Uploading....");
        Bundle bundle = this.getIntent().getExtras();
        String idusuario;
        if (bundle != null) {
            idusuario = bundle.getString("id");
        } else {
            idusuario = "";
        }
        databaseReference.child("Usuarios").child(idusuario).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //String userType = dataSnapshot.child("tiporegistro").getValue().toString();
                if (dataSnapshot.exists()) {
                    if (dataSnapshot.child("nombre").exists()) {
                        nombre = dataSnapshot.child("nombre").getValue().toString();
                        NombreProfil.setText(nombre);
                    } else {
                        NombreProfil.setText("");
                    }
                    if (dataSnapshot.child("celular").exists()) {
                        celular = dataSnapshot.child("celular").getValue().toString();
                        CelularProfil.setText(celular);
                    } else {
                        CelularProfil.setText("");
                    }
                    if (dataSnapshot.child("email").exists()) {
                        email = dataSnapshot.child("email").getValue().toString();
                        EmailProfil.setText(email);
                    } else {
                        EmailProfil.setText("");
                    }
                    if (dataSnapshot.child("imagen").exists()) {
                        imagen = dataSnapshot.child("imagen").getValue().toString();
                        Picasso.get().load(imagen).into(ImgUserPhoto);
                    } else {
                        Picasso.get().load(R.drawable.img_no_disponible).into(ImgUserPhoto);
                    }
                    if (dataSnapshot.child("tiporegistro").exists()) {
                        tiporegistro = dataSnapshot.child("tiporegistro").getValue().toString();
                        TipoRegistroProfil.setText(String.format("Perfil de %s", tiporegistro));
                    } else {
                        tiporegistro = "";
                        TipoRegistroProfil.setText("");
                    }
                    if (dataSnapshot.child("lenguaje").exists()) {
                        lenguaje = dataSnapshot.child("lenguaje").getValue().toString();
                        LenguajeProfil.setText(lenguaje);
                    } else {
                        // LenguajeProfil.setText("");
                        LinLeng.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("experiencia").exists()) {
                        experiencia = dataSnapshot.child("experiencia").getValue().toString();
                        ExperienciaProfil.setText(experiencia);
                    } else {
                        //  LenguajeProfil.setText("");
                        LinExp.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("recomendaciones").exists()) {
                        recomendaciones = dataSnapshot.child("recomendaciones").getValue().toString();
                        RecomendacionesProfil.setText(recomendaciones);
                    } else {
                        //    RecomendacionesProfil.setText("");
                        LinRec.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("direccion").exists()) {
                        direccion = dataSnapshot.child("direccion").getValue().toString();
                        DireccionProfil.setText(direccion);
                    } else {
                        //   DireccionProfil.setText("");
                        LinDir.setVisibility(View.GONE);
                    }
                    //Glide.with(this).load(imagen).into(ImgUserPhoto);
                } else {
                    Toast.makeText(ActivityPerfilPublico.this, "No se encuentra el  perfil", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
            }
        });
    }
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(ActivityPerfilPublico.this, ActivityPrincipal.class));
    }
}
