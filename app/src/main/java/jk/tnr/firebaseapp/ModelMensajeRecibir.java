package jk.tnr.firebaseapp;
class ModelMensajeRecibir extends ModelMensaje {
    private Long hora;
    public ModelMensajeRecibir() {
    }
    public ModelMensajeRecibir(Long hora) {
        this.hora = hora;
    }
    public ModelMensajeRecibir(String mensaje, String urlFoto, String nombre, String fotoPerfil, String type_mensaje, Long hora, String idmensaje, String keyEmisor) {
        super(mensaje, urlFoto, nombre, fotoPerfil, type_mensaje, idmensaje, keyEmisor);
        this.hora = hora;
    }
    public Long getHora() {
        return hora;
    }
    public void setHora(Long hora) {
        this.hora = hora;
    }
}
