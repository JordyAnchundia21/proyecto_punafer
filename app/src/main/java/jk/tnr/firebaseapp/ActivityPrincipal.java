package jk.tnr.firebaseapp;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Matrix;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import android.widget.ImageView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.firebase.auth.FirebaseAuth;
public class ActivityPrincipal extends AppCompatActivity {
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    PendingIntent pending;
    ImageView imageView;
    Matrix matrix = new Matrix();
    Float scale = 1f;
    ScaleGestureDetector SGD;
    private String mensaje;
    private int NOTIFICACION_ID;
    private MediaPlayer song;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);
 MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        AdView mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("16B879AF647EB644CC229CEA3CC82FC2").build();
        mAdView.loadAd(adRequest);
    /*    FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
       if (firebaseUser == null) {
            startActivity(new Intent(ActivityPrincipal.this, ActivityIniciarSesion.class));
        }*/
        Button regNorm = findViewById(R.id.reg);
        Button mur = findViewById(R.id.muro);
        Button profil = findViewById(R.id.profile);
        Button iniciosesion = findViewById(R.id.login_btn);
        Button list = findViewById(R.id.list);
        Button btnCerrarSesion = findViewById(R.id.btnCerrarSesion);
        song = MediaPlayer.create(this, R.raw.sound);
        NOTIFICACION_ID = 2;
        mensaje = "Bienvenido ";
        profil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPrincipal.this, ActivityProfile.class));
            }
        });
        regNorm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPrincipal.this, ActivityRegister.class));
            }
        });
        iniciosesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPrincipal.this, ActivityIniciarSesion.class));
            }
        });
        list.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPrincipal.this, ActivityListUsers.class));
            }
        });
        mur.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityPrincipal.this, ActivityMuro.class));
            }
        });
        btnCerrarSesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog dialog;
                ProgressDialog pd;
                dialog = new AlertDialog.Builder(ActivityPrincipal.this).create();
                pd = new ProgressDialog(ActivityPrincipal.this);
                pd.setMessage("Uploading....");
                dialog.setTitle("Cerrar sesión");
                dialog.setButton(DialogInterface.BUTTON_POSITIVE, "Aceptar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FirebaseAuth.getInstance().signOut();
                        startActivity(new Intent(ActivityPrincipal.this, ActivityIniciarSesion.class));
                    }
                });
                dialog.show();
            }
        });
        imageView = (ImageView) findViewById(R.id.logo);
        SGD = new ScaleGestureDetector(this, new ScaleListener());
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        SGD.onTouchEvent(event);
        return true;
    }
    private void crearNotificacion() {
        String CHANNEL_ID = "Notificacion";
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this, CHANNEL_ID);
        builder.setSmallIcon(R.drawable.icono_launcher_foreground);
        builder.setContentTitle("Punafer");
        builder.setContentText(mensaje);
        builder.setTicker("Nueva Notificación");
        builder.setPriority(NotificationCompat.PRIORITY_DEFAULT);
        builder.setLights(Color.RED, 1000, 1000);
        builder.setAutoCancel(true);
        song.start();
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(this);
        notificationManagerCompat.notify(NOTIFICACION_ID, builder.build());
    }
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
    }
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            scale = scale * detector.getScaleFactor();
            scale = Math.max(0.1f, Math.min(scale, 5f));
            matrix.setScale(scale, scale);
            imageView.setImageMatrix(matrix);
            return true;
        }
    }
}
/*
Intent intent = new Intent(ActivityPrincipal.this, ActivityRegister.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(ActivityPrincipal.this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
     */