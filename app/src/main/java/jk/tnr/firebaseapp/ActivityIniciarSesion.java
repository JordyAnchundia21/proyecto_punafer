package jk.tnr.firebaseapp;
import android.app.ProgressDialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
public class ActivityIniciarSesion extends AppCompatActivity {
    private static long presionado = 2000;
    String IDUSUARIO = DatosUsuario.getInstancia().IdUsuario();
    private EditText email;
    private EditText passwd;
    private ProgressDialog pd;
    private FirebaseAuth firebaseAuth;
    private MediaPlayer song;
    private long primerclick;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iniciar_sesion);
        song = MediaPlayer.create(this, R.raw.sound);
        pd = new ProgressDialog(this);
        pd.setMessage("Uploading....");
        pd.setIcon(R.drawable.img_logo);
        pd.setProgressStyle(R.drawable.progress_bar_style);
        email = findViewById(R.id.emaillogin);
        passwd = findViewById(R.id.contrasenalogin);
        TextView registrarse = findViewById(R.id.registrarse);
        registrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ActivityIniciarSesion.this, ActivityRegister.class));
            }
        });
        Button iniciarsesion = findViewById(R.id.botoniniciarsesion);
        firebaseAuth = FirebaseAuth.getInstance();
        iniciarsesion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String emaillogin = email.getText().toString().trim();
                String contrasenalogin = passwd.getText().toString().trim();
                if (emaillogin.isEmpty() || contrasenalogin.isEmpty()) {
                    Toast.makeText(ActivityIniciarSesion.this, "Complete los campos", Toast.LENGTH_LONG).show();
                    song.start();
                } else {
                    pd.show();
                    iniciarSesion(emaillogin, contrasenalogin);
                }
            }
        });
    }
    private void iniciarSesion(String emaillogin, String contrasenalogin) {
        firebaseAuth.signInWithEmailAndPassword(emaillogin, contrasenalogin).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    startActivity(new Intent(ActivityIniciarSesion.this, ActivityMuro.class));
                } else {
                    pd.dismiss();
                    Toast.makeText(ActivityIniciarSesion.this, "Verifique que los datos ingresados sean correctos o que usted este registrado", Toast.LENGTH_LONG).show();
                    song.start();
                }
            }
        });
    }
    public void onBackPressed() {
        //super.onBackPressed();
        startActivity(new Intent(ActivityIniciarSesion.this, ActivityPrincipal.class));
    }
}