package jk.tnr.firebaseapp;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
public class AdapterListUsers extends RecyclerView.Adapter<AdapterListUsers.MyViewHolder> {
    private final Context context;
    private final ArrayList<ModelListUsers> modelListUsers;
    Intent intent;
    AdapterListUsers(Context c, ArrayList<ModelListUsers> p) {
        context = c;
        modelListUsers = p;
    }
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.cardview_listusers, viewGroup, false));
    }
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        position = ((getItemCount()) - (position + 1));
        holder.nombre.setText(modelListUsers.get(position).getNombre());
        holder.email.setText(modelListUsers.get(position).getEmail());
        Picasso.get().load(modelListUsers.get(position).getImagen()).into(holder.image);
        holder.tiporegistro.setText(modelListUsers.get(position).getTiporegistro());
        holder.IdUser.setText(modelListUsers.get(position).getIdusuario());
        final int finalPosition = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityMensajes.class);
                Bundle bundle = new Bundle();
                bundle.putString("id", modelListUsers.get(finalPosition).getIdusuario());
                bundle.putString("nombre", modelListUsers.get(finalPosition).getNombre());
                bundle.putString("email", modelListUsers.get(finalPosition).getEmail());
                bundle.putString("imagen", modelListUsers.get(finalPosition).getImagen());
                // método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return modelListUsers.size();
    }
    static class MyViewHolder extends RecyclerView.ViewHolder {
        final CircleImageView image;
        final TextView nombre;
        final TextView email;
        final TextView tiporegistro;
        final TextView IdUser;
        TextView verList;
        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            image = itemView.findViewById(R.id.imgList);
            nombre = itemView.findViewById(R.id.nameList);
            email = itemView.findViewById(R.id.emailList);
            tiporegistro = itemView.findViewById(R.id.tipRegList);
            IdUser = itemView.findViewById(R.id.iduserList);
        }
    }
}
/*   final int finalPosition = position;
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ActivityPerfilPublico.class);
                Bundle bundle = new Bundle();
                bundle.putString("", modelListUsers.get(finalPosition).getIdusuario());
                // método putExtras envia un objeto de tipo bundle como un sólo parámetro entre actividades
                intent.putExtras(bundle);
                view.getContext().startActivity(intent);
            }
        });*/