package jk.tnr.firebaseapp;
class ModelMuro {
    ModelAnuncio modelAnuncio;
    ModelListUsers modelListUsers;
    public ModelMuro() {
    }
    public ModelMuro(ModelAnuncio modelAnuncio, ModelListUsers modelListUsers) {
        this.modelAnuncio = modelAnuncio;
        this.modelListUsers = modelListUsers;
    }
    public ModelAnuncio getModelAnuncio() {
        return modelAnuncio;
    }
    public void setModelAnuncio(ModelAnuncio modelAnuncio) {
        this.modelAnuncio = modelAnuncio;
    }
    public ModelListUsers getModelListUsers() {
        return modelListUsers;
    }
    public void setModelListUsers(ModelListUsers modelListUsers) {
        this.modelListUsers = modelListUsers;
    }
}