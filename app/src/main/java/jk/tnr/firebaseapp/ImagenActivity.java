package jk.tnr.firebaseapp;
import android.os.Bundle;
import android.widget.ImageView;

import com.jsibbold.zoomage.ZoomageView;

import androidx.appcompat.app.AppCompatActivity;

import com.squareup.picasso.Picasso;
public class ImagenActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_imagen);
       ZoomageView  imagen = findViewById(R.id.imgShow);
        Bundle bundle = this.getIntent().getExtras();
        if (bundle != null) {
            String imagenBundle = bundle.getString("imagen");
            Picasso.get().load(imagenBundle).into(imagen);
        }
    }
}
