package jk.tnr.firebaseapp;
import java.util.Map;
class ModelMensajeEnviar extends ModelMensaje {
    private Map hora;
    public ModelMensajeEnviar() {
    }
    public ModelMensajeEnviar(Map hora) {
        this.hora = hora;
    }
    public ModelMensajeEnviar(String mensaje, String nombre, String fotoPerfil, String type_mensaje, Map hora, String idmensaje, String keyEmisor) {
        super(mensaje, nombre, fotoPerfil, type_mensaje, idmensaje, keyEmisor);
        this.hora = hora;
    }
    public ModelMensajeEnviar(String mensaje, String urlFoto, String nombre, String fotoPerfil, String type_mensaje, Map hora, String idmensaje, String keyEmisor) {
        super(mensaje, urlFoto, nombre, fotoPerfil, type_mensaje, idmensaje, keyEmisor);
        this.hora = hora;
    }
    public Map getHora() {
        return hora;
    }
    public void setHora(Map hora) {
        this.hora = hora;
    }
}
