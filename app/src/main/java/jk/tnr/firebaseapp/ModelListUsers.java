package jk.tnr.firebaseapp;
class ModelListUsers {
    private String imagen;
    private String nombre;
    private String email;
    private String tiporegistro;
    private String idusuario;
    public ModelListUsers() {
    }
    public ModelListUsers(String imagen, String nombre, String email, String tiporegistro, String idusuario) {
        this.imagen = imagen;
        this.nombre = nombre;
        this.email = email;
        this.tiporegistro = tiporegistro;
        this.idusuario = idusuario;
    }
    public String getImagen() {
        return imagen;
    }
    public void setImagen(String imagen) {
        this.imagen = imagen;
    }
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getTiporegistro() {
        return tiporegistro;
    }
    public void setTiporegistro(String tiporegistro) {
        this.tiporegistro = tiporegistro;
    }
    public String getIdusuario() {
        return idusuario;
    }
    public void setIdusuario(String idusuario) {
        this.idusuario = idusuario;
    }
}
