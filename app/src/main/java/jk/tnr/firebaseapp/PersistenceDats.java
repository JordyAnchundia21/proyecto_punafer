package jk.tnr.firebaseapp;
import com.google.firebase.database.FirebaseDatabase;
public class PersistenceDats extends android.app.Application {
    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseDatabase.getInstance().setPersistenceEnabled(true);
    }
}